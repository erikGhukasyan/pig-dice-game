let player1Turn = true;
let currentScore1 = 0;
let currentScore2 = 0;
let score1 = 0;
let score2 = 0;


function rollDice() {
    let randomNum = Math.floor(Math.random() * Math.floor(6))+1;

    document.getElementById("dice").innerHTML = '<img class="diceImg" src="images/dice-' + randomNum + '.png" alt="">';

    if(player1Turn) {

        if(randomNum === 1) {
            document.getElementById('player1').style.boxShadow = '0px 0px 0px black';
            document.getElementById('player2').style.boxShadow = '1px 1px 5px black';
            document.getElementById('player1').style.backgroundColor = 'white';
            document.getElementById('player2').style.backgroundColor = '#ECECEC';
            currentScore1 = 0;
            player1Turn = false;

        }
        else
            currentScore1 += randomNum;
        updateValue("currentScore1", currentScore1);
    }
    else {
        if(randomNum === 1) {
            document.getElementById('player2').style.boxShadow = '0px 0px 0px black';
            document.getElementById('player1').style.boxShadow = '1px 1px 5px black';
            document.getElementById('player2').style.backgroundColor = 'white';
            document.getElementById('player1').style.backgroundColor = '#ECECEC';
            currentScore2 = 0;
            player1Turn = true;
        }
        else
            currentScore2 += randomNum;
        updateValue("currentScore2", currentScore2);
    }
    return randomNum;
}

function hold() {
    if(player1Turn) {
        document.getElementById('player1').style.transitionDuration = '0.8s';
        document.getElementById('player2').style.transitionDuration = '0.8s';
        document.getElementById('player1').style.boxShadow = '0px 0px 0px black';
        document.getElementById('player2').style.boxShadow = '1px 1px 5px black';
        document.getElementById('player1').style.backgroundColor = 'white';
        document.getElementById('player2').style.backgroundColor = '#ECECEC';
        score1 += currentScore1;
        player1Turn = false;
        currentScore1 = 0;
        updateValue("score1", score1);
        if(score1 >= 100) {
            gameOver('Player 1');
        }
    }
    else {
        document.getElementById('player2').style.transitionDuration = '0.8s';
        document.getElementById('player2').style.boxShadow = '0px 0px 0px black';
        document.getElementById('player1').style.boxShadow = '1px 1px 5px black';
        document.getElementById('player2').style.backgroundColor = 'white';
        document.getElementById('player1').style.backgroundColor = '#ECECEC';
        score2 += currentScore2;
        player1Turn = true;
        currentScore2 = 0;
        updateValue("score2", score2);
        if(score2 >= 100) {
            gameOver('Player 2');
        }
    }
}

function updateValue(id, value) {
    document.getElementById(id).innerHTML = value;
}
function gameOver(player) {
        if(confirm(player + " à gagné, RESTART?")) {
            updateValue("score2", 0) ;
            updateValue("score1", 0) ;
            updateValue("currentScore1", 0) ;
            updateValue("currentScore2", 0) ;
            player1Turn = true;
            document.getElementById('player2').style.boxShadow = '0px 0px 0px black';
            document.getElementById('player1').style.boxShadow = '1px 1px 5px black';
            document.getElementById('player2').style.backgroundColor = 'white';
            document.getElementById('player1').style.backgroundColor = '#ECECEC';
        }
        else {
            close();
        }
}
